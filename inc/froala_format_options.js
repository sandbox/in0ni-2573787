/**
 * @file
 * Default options per text format.
 */

Drupal.froala = Drupal.froala || {};

Drupal.froala.format_options = {
  /*
   * Text format: plain_text.
   *
   * - comes by default with drupal
   * - is DISABLED by default for froala
   *
   * by default we disable froala when using the plain_text format if you
   * want to *enable* froala pass it an object with settings:
   *
   * 'plain_text' : { option: value },
   *
   * setting plain text to false, or removing the line both work.
   *
   */
  plain_text: false,

  /*
   * Text format: filtered_html.
   *
   * - comes by default with drupal
   * - is ENABLED by default for froala
   *
   * if you want to *disable* froala for this filter, set it to *false*:
   *
   * 'filtered_html' : false,
   *
   * if you want to modify the loading options for 'filtered_html' modify
   * the options below to your liking.
   *
   * see the available options online:
   * <http://froala.com/wysiwyg-editor/docs/options>.
   */

  filtered_html: {
    htmlAllowComments: false,
    htmlAllowedAttrs: ['title', 'href', 'alt', 'src', 'class', 'target'],
    htmlAllowedEmptyTags: [],
    htmlAllowedTags: ['h1', 'h2', 'h3', 'h4', 'p', 'ol', 'ul', 'li', 'a', 'strong', 'em', 'b', 'i'],
    paragraphFormat: {
      n: 'Normal',
      h1: 'Heading 1',
      h2: 'Heading 2',
      h3: 'Heading 3',
      h4: 'Heading 4',
      h5: 'Heading 5'
    },
    toolbarButtons: ['undo', 'redo', '|', 'bold', 'italic', '|', 'formatOL', 'formatUL', '|', 'insertLink'],
    toolbarInline: false,
    heightMin: 200,
    pastePlain: true,
    tabSpaces: false
  }

  /*
   * Adding custom formats.
   *
   * if you want froala to load when using a custom filter, simply add another
   * property with the name being machine name of your drupal text format.
   * For example if your text format machine name is 'custom_filter':
   *
   * 'custom_filter': { option: value },
   *
   * if the object is empty or does not contain valid froala options an error
   * message printed to the javascript console.
   */
};

/*
 * Advanced Examples.
 *
 * below are two examples of how to modify the configuration depending
 * on classes found on the page
 *
 *  1. add 'formatBlock' to node add/edit pages
 *  2. add 'html' button only (code view) for admin
 *
 * some hooks that can help to add classes to your drupal pages:
 *
 *   @see hook_form_alter
 *   @see hook_form_FORM_ID_alter
 *   @see template_process_html
 *   @see template_process_page
 *
 * for full customization options see froala's docs:
 * <http://froala.com/wysiwyg-editor/docs>.
 */

/*
(function($) {
  Drupal.behaviors.alterOptions ={
    attach: function(context, settings) {
      var body = $('body');
      // [1]. add format block
      if (body.hasClass('page-node-add') || body.hasClass('page-node-edit')) {
        if (Drupal.froala.format_options.filtered_html.buttons.indexOf('formatBlock') == -1) {
          Drupal.froala.format_options.filtered_html.buttons.unshift('formatBlock');
        }
      }
      // [2]. let admin use html code button
      if ($('body.is-admin').length) {
        if (Drupal.froala.format_options.filtered_html.buttons.indexOf('html') == -1) {
          Drupal.froala.format_options.filtered_html.buttons.push('sep');
          Drupal.froala.format_options.filtered_html.buttons.push('html');
        }
      }
    }// attach()
  };
})(jQuery);
*/
