/**
 * @file
 * Attach/detach froala instances, and handle changing of text format.
 */

Drupal.froala = Drupal.froala || {};
Drupal.froala.instances = Drupal.froala.instances || {};

Drupal.froala.selectors = {
  textarea: '.froala-textarea',
  select: '.froala-format-select'
};

(function($) {

  Drupal.behaviors.bindFroala = {
    attach: function (context, settings) {
      // License.
      $.FroalaEditor.DEFAULTS.key = Drupal.settings.froala.key;

      // Attach all existing textareas if applicable.
      $(Drupal.froala.selectors.textarea, context).each(function() {
        Drupal.froala.instance_attach(this);
      });

      // Bind to format selection change event to load/unload editor.
      $(Drupal.froala.selectors.select, context).change(function(ev, el) {
        Drupal.froala.instance_toggle(this, context);
      });

    },
    detach: function (context, settings, trigger) {

      // Detach all textareas if applicable.
      $(Drupal.froala.selectors.textarea, context).each(function() {
        Drupal.froala.instance_detach(this, trigger);
      });
    }
  };

  Drupal.froala.instance_attach = function(el) {
    var textarea = $(el);
    var textarea_id = textarea.attr('id');
    var textarea_format = textarea.attr('data-format');

    // Only if hasn't already been initialized.
    if (!textarea.data('fa.froalaEditor') && typeof Drupal.froala.instances[textarea_id] === 'undefined') {

      // Don't continue if format_options isn't set.
      if (typeof Drupal.froala.format_options == 'undefined') {
        $.error('(Froala Drupal Module) Drupal.froala.format_options is undefined');
      }

      // Only if enabled in Drupal.froala.format_options.
      if (typeof Drupal.froala.format_options[textarea_format] === 'object') {
        Drupal.froala.instances[textarea_id] = textarea.froalaEditor(Drupal.froala.format_options[textarea_format]);

        /*
         * @todo
         *  by default drupal disables the comment submit:
         *  <https://www.drupal.org/node/2359063>
         *  also ckeditor has code to pass events and refers to:
         *  <https://www.drupal.org/node/1895278>
         *  was unsuccessful at firing events
         */
        Drupal.froala.instances[textarea_id].on('editable.focus', function(e, editor) {
          var this_el = $(editor.$element);
          var parent_form = this_el.parents('form');

          if (parent_form.attr('id') === 'comment-form') {
            var disabled_submit = parent_form.find('.form-disabled[type="submit"]');

            if (disabled_submit.prop('disabled')) {
              disabled_submit.removeProp('disabled');
              disabled_submit.removeClass('form-disabled');
            }
          }
        });

      }
    }// Only if not already loaded.
  };

  Drupal.froala.instance_detach = function(el, trigger) {
    var textarea = $(el);
    var textarea_id = textarea.attr('id');
    var method = (trigger === 'serialize') ? 'sync' : 'destroy';

    if (typeof Drupal.froala.instances[textarea_id] === 'undefined') {
      return;
    }
    else {
      if (textarea.data('fa.froalaEditor')) {
        textarea.froalaEditor(method);
        delete Drupal.froala.instances[textarea_id];
      }
    }
  };

  Drupal.froala.instance_toggle = function(el, context) {
    var select = $(el);
    var textarea_name = select.attr('name').replace(/\[format\]$/, '[value]');
    var textarea = $('textarea[name="' + textarea_name + '"]', context);

    // This is where the magic happens.
    textarea.attr('data-format', select.val());
    // Unload is defined in drupal.js.
    Drupal.froala.instance_detach(textarea[0], 'unload');
    Drupal.froala.instance_attach(textarea[0]);
  };

  // Respond to CTools detach behaviors event.
  $(document).bind('CToolsDetachBehaviors', function(event, context) {
    Drupal.behaviors.attachWysiwyg.detach(context, {}, 'unload');
  });
})(jQuery);
