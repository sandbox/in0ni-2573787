<?php
/**
 * @file
 * Module administration config.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function froala_admin_settings($form) {
  $plugin_options = array(
    'align' => t('Align - Adds the align option'),
    'char_counter' => t('Char Counter - limits the number of characters that can be inserted in the editor'),
    'code_view' => t('Code View - Enables code view for the editor content'),
    'colors' => t('Background & Text Colors - adds in the possibility to change the background and text colors'),
    'emoticons' => t('Emoticons - make your users smile'),
    'entities' => t('Entities - Converts characters to special HTML entities'),
    'font_family' => t('Font Family - allows users to select from different font types'),
    'font_size' => t('Font Size - allows users to change the font size with pixel precision'),
    'fullscreen' => t('Fullscreen - add fullscreen option'),
    'inline_style' => t('Inline Styles - define custom styles for selected text'),
    'line_breaker' => t('Line Breaker - Helper to add new lines between elements such as tables'),
    'link' => t('Link - Enables advanced link editing'),
    'lists' => t('Lists - adds in the lists buttons for inserting simple lists or nested lists'),
    'paragraph_format' => t('Paragraph Format - Allows users to change the type of a paragraph'),
    'paragraph_style' => t('Paragraph Style - Allows user to choose a style for a paragraph'),
    'quote' => t('Quote - Adds quote option.'),
    'table' => t('Tables - basic and advanced operations on cells, rows and columns'),
    'url' => t('URL - Convert text to URL as you type'),
    'video' => t('Video - easily insert videos by URL or by embedded code'),
  );

  $form['key'] = array(
    '#type' => 'fieldset',
    '#title' => t('License Key'),
    '#description' => t('Enter the license key generated in your froala.com account for this domain, leaving it blank displays "unlicensed" on interface.'),
    'froala_key' => array(
      '#type' => 'textfield',
      '#title' => t('Matching Domain Key'),
      '#default_value' => variable_get('froala_key', ''),
    ),
  );

  $form['plugins'] = array(
    '#type' => 'fieldset',
    '#title' => t('Plugins'),
    '#description' => t('Check the plugin to load it'),
    'froala_enabled_plugins' => array(
      '#type' => 'checkboxes',
      '#title' => t('Enabled Plugins'),
      '#default_value' => variable_get('froala_enabled_plugins', array()),
      '#options' => $plugin_options,
    ),
  );

  return system_settings_form($form);
}
