-
= DEFAULT BEHAVIOR
------------------
This module will only attach itself to "filtered_html" text format by default,
it will not attach itself to "plain_text" format. To customize this behavior
please take a look at "CUSTOMIZING FROALA" below.


-
= REQUIREMENTS
--------------

before installing this module you must download/install:

  1. Libraries API
  2. Froala WYSIWYG library (v2)
  3. Font Awesome library (required by froala library)
  4. jQuery Update module (required by froala library)

1. Libraries API
   <https://www.drupal.org/project/libraries>

2. Froala WYSIWYG library v2
   <https://www.froala.com/wysiwyg-editor/>

  ************** ensure you are downloading v2, and not v1 **************

    as of oct 2015 you can only get v2RC via git (expected release soon):
    https://github.com/froala/wysiwyg-editor.git

  ***********************************************************************

  * download the library, and extract in your libraries folder:
    sites/all/libraries/froala
  * make sure 'package.json' is found at the root level:
    sites/all/libraries/froala/package.json

3. Font Awesome library
   <https://fortawesome.github.io/Font-Awesome/>

  * download the library, and extract in your libraries folder: 
    sites/all/libraries/font_awesome
  * make sure 'font-awesome.css' is found here: 
    sites/all/libraries/font_awesome/css/font-awesome.css

4. jquery_update
   <https://www.drupal.org/project/jquery_update>

The Froala WYSIWYG library requires jQuery v1.11+, Drupal 7 is shipped with
v1.4. The best way to upgrade, and therefore a requirement for this module,
is the jquery_update module.

  * (via drush) drush dl jquery_update && drush en -y jquery_update
  * configure jquery_update to make sure the default version is 1.11


-
= INSTALLATION
--------------

This module will only attach itself to "filtered_html" text format by default,
it will not attach itself to "plain_text" format. To customize this behavior
please take a look at "CUSTOMIZING FROALA" below.

Install module either manually or via drush:

  * drush en -y froala (or download to your modules directory)
  * enter your license and configure froala here: admin/config/content/froala


= CUSTOMIZING FROALA (advanced)
-------------------------------

froala documentation: <http://froala.com/wysiwyg-editor/docs>

  in order to overwrite default configuration, customize froala to fit your
  needs, or attach to your custom text formats simply:

    1. **COPY**
       {drupal_path}/sites/all/modules/froala/froala_format_options.js
    2. and **RENAME** to
       {drupal_path}/sites/all/libraries/froala/drupal_format_options.js
    3. take a look at the comments in this file for how to fully customize the
       the editor.

  once this file exists (drupal_format_options.js), the module will load your
  configuration instead of the default.
